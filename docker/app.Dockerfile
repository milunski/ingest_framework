FROM python:latest
LABEL maintainer="Matt Milunski" email="matt.milunski@slalom.com"

WORKDIR /ingest
COPY . .
RUN python -m pip install -r requirements.txt
CMD python main.py \
    --bcda_url $BCDA_URL \
    --fhir_resource $FHIR_RESOURCE \
    --gcs_bucket $GCS_BUCKET \
    --bucket_prefix $BUCKET_PREFIX \
    --partition_time $PARTITION_TIME \
    --project $PROJECT \
    --secret_id $SECRET_ID
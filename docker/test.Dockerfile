# Beam Python SDK supports up to Python 3.8: https://beam.apache.org/get-started/quickstart-py/

FROM python:latest
LABEL maintainer="Matt Milunski" email="matt.milunski@slalom.com"

WORKDIR /ingest
COPY . .
RUN python -m pip install -r requirements.txt pytest
CMD pytest -vv

"""Configuration tests"""

import os
from pydantic import ValidationError
import pytest
from src.config import Settings, arguments


def test_arguments(env_vars):
    vars = arguments(
        [
            "--bcda_url",
            "BCDA_URL",
            "--fhir_resource",
            "FHIR_RESOURCE",
            "--gcs_bucket",
            "GCS_BUCKET",
            "--bucket_prefix",
            "BUCKET_PREFIX",
            "--partition_time",
            "PARTITION_TIME",
            "--project",
            "PROJECT",
            "--secret_id",
            "SECRET_ID",
        ]
    )
    assert os.environ[vars.gcs_bucket] == "LandingZone"


def test_valid_explanation_of_benefit(env_vars):
    vars = arguments(
        [
            "--bcda_url",
            "BCDA_URL",
            "--fhir_resource",
            "FHIR_RESOURCE",
            "--gcs_bucket",
            "GCS_BUCKET",
            "--bucket_prefix",
            "BUCKET_PREFIX",
            "--partition_time",
            "PARTITION_TIME",
            "--project",
            "PROJECT",
            "--secret_id",
            "SECRET_ID",
        ]
    )
    settings = Settings(
        fhir_resource=os.environ[vars.fhir_resource],
        bcda_url=os.environ[vars.bcda_url],
        client_id="foo",
        client_secret="bar",
        gcs_bucket=os.environ[vars.gcs_bucket],
        bucket_prefix=os.environ[vars.bucket_prefix],
        partition_time=os.environ[vars.partition_time],
        project=os.environ[vars.project],
    )
    assert settings.fhir_resource == "ExplanationOfBenefit"


def test_invalid_explanation_of_benefit(bad_env_vars):
    vars = arguments(
        [
            "--bcda_url",
            "BCDA_URL",
            "--fhir_resource",
            "FHIR_RESOURCE",
            "--gcs_bucket",
            "GCS_BUCKET",
            "--bucket_prefix",
            "BUCKET_PREFIX",
            "--partition_time",
            "PARTITION_TIME",
            "--project",
            "PROJECT",
            "--secret_id",
            "SECRET_ID",
        ]
    )
    with pytest.raises(ValidationError, match=r"type=value_error.const"):
        Settings(
            fhir_resource=os.environ[vars.fhir_resource],
            bcda_url=os.environ[vars.bcda_url],
            client_id="foo",
            client_secret="bar",
            gcs_bucket=os.environ[vars.gcs_bucket],
            bucket_prefix=os.environ[vars.bucket_prefix],
            partition_time=os.environ[vars.partition_time],
            project=os.environ[vars.project],
        )

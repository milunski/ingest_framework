"""Tests for APi requests"""

import os
import pytest
from src import api_caller
from src import config


@pytest.fixture
def full_settings(env_vars) -> config.Settings:
    vars = config.arguments(
        [
            "--bcda_url",
            "BCDA_URL",
            "--fhir_resource",
            "FHIR_RESOURCE",
            "--gcs_bucket",
            "GCS_BUCKET",
            "--bucket_prefix",
            "BUCKET_PREFIX",
            "--partition_time",
            "PARTITION_TIME",
            "--secret_id",
            "SECRET_ID",
        ]
    )
    settings = config.Settings(
        fhir_resource=os.environ[vars.fhir_resource],
        bcda_url=os.environ[vars.bcda_url],
        client_id="foo",
        client_secret="bar",
        gcs_bucket=os.environ[vars.gcs_bucket],
        bucket_prefix=os.environ[vars.bucket_prefix],
        partition_time=os.environ[vars.partition_time],
    )
    return settings


def test_AccessTokenRequest(full_settings):
    encoded_auth = "Zm9vOmJhcg=="
    auth_request = api_caller.AccessTokenRequest(user=full_settings)
    assert auth_request.user == encoded_auth


def test_url(full_settings):
    eob_url = "https://sandbox.bcda.cms.gov/api/v2/Group/all/$export?_type=ExplanationOfBenefit"
    jobs_url = "https://sandbox.bcda.cms.gov/api/v2/jobs"
    auth_url = "https://sandbox.bcda.cms.gov/auth/token"
    assert (
        api_caller.url(
            endpoint=api_caller.JobStartRequest(authorization="123"),
            settings=full_settings,
        )
        == eob_url
    )
    assert (
        api_caller.url(
            endpoint=api_caller.JobStatusRequest(authorization="123"),
            settings=full_settings,
        )
        == jobs_url
    )
    assert (
        api_caller.url(
            endpoint=api_caller.AccessTokenRequest(user=full_settings),
            settings=full_settings,
        )
        == auth_url
    )
    with pytest.raises(ValueError):
        api_caller.url(settings=full_settings, endpoint="fooBar") == auth_url


def test_extract_filename():
    observed = "https://sandbox.bcda.cms.gov/data/31673/6ae21058-45cb-4ea1-b822-73cb16100e44.ndjson"
    expected = "6ae21058-45cb-4ea1-b822-73cb16100e44"
    assert api_caller.extract_filename(observed) == expected

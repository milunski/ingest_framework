"""Testing fixtures"""

import os
from unittest import mock
import pytest


@pytest.fixture
def env_vars():
    env_vars = {
        "BCDA_URL": "https://sandbox.bcda.cms.gov/",
        "FHIR_RESOURCE": "ExplanationOfBenefit",
        "GCS_BUCKET": "LandingZone",
        "BUCKET_PREFIX": "explanation_of_benefit",
        "PARTITION_TIME": "2022_05_09",
        "PROJECT": "gcp-stl",
        "SECRET_ID": "project/123/"
    }
    with mock.patch.dict(os.environ, env_vars, clear=True):
        yield


@pytest.fixture
def bad_env_vars():
    env_vars = {
        "BCDA_URL": "https://sandbox.bcda.cms.gov/",
        "FHIR_RESOURCE": "BenefitOfExplanation",
        "GCS_BUCKET": "LandingZone",
        "BUCKET_PREFIX": "explanation_of_benefit",
        "PARTITION_TIME": "2022_05_09",
        "PROJECT": "gcp-stl",
        "SECRET_ID": "project/123/"
    }
    with mock.patch.dict(os.environ, env_vars, clear=True):
        yield

"""Main function"""

import json
import os
import sys
import threading
from typing import Dict, Text
from pydantic import AnyHttpUrl
import requests
from google.cloud import storage, secretmanager
from src.config import Settings, arguments
from src.api_caller import (
    AccessTokenRequest,
    JobStartRequest,
    check_job_status,
    download_data,
    extract_filename,
    url,
    write_gcs,
)


def get_token(config: Settings) -> Text:
    """Retrieves a new token"""
    auth_request_body = AccessTokenRequest(
        endpoint=url(settings=config, endpoint=AccessTokenRequest(user=config)),
        user=config,
    )
    auth_response = requests.post(
        url=auth_request_body.endpoint,
        headers={
            "accept": auth_request_body.accept,
            "Content-type": auth_request_body.accept,
            "Authorization": f"Basic {auth_request_body.user}",
        },
    )
    auth_response.raise_for_status
    token = auth_response.json()["access_token"]
    return token


def main():
    # Read CLI args and create Google clients
    args = arguments(sys.argv[1:])
    gcs_client = storage.Client()
    gsm_client = secretmanager.SecretManagerServiceClient()

    # Retrieve secret and build configs
    secret_payload = gsm_client.access_secret_version(
        request={"name": args.secret_id}
    )
    config = Settings(
        fhir_resource=args.fhir_resource,
        bcda_url=args.bcda_url,
        client_id=json.loads(secret_payload.payload.data)["client_id"],
        client_secret=json.loads(secret_payload.payload.data)["client_secret"],
        gcs_bucket=args.gcs_bucket,
        bucket_prefix=args.bucket_prefix,
        partition_time=args.partition_time,
        project=args.project,
    )

    # Generate URL to get token
    token = get_token(config)

    # Start job, extract Content-Location
    job_start_request_body = JobStartRequest(
        endpoint=url(config, JobStartRequest()), authorization=token
    )
    job_start_response = requests.get(
        job_start_request_body.endpoint,
        headers={
            "accept": job_start_request_body.accept,
            "prefer": job_start_request_body.prefer,
            "Authorization": f"Bearer {job_start_request_body.authorization}",
        },
    )
    job_start_response.raise_for_status
    content_location: AnyHttpUrl = job_start_response.headers[
        "Content-Location"
    ]

    # Poll the URL and extract output
    job_status_response = check_job_status(
        content_location, job_start_request_body
    )
    print(job_start_response.text)
    try:
        output: Dict = json.loads(job_status_response.content.decode())[
            "output"
        ]
    except KeyError as e:
        if "Invalid Token" in str(job_status_response):
            token = get_token(config)
            job_status_response = check_job_status(
                content_location, job_start_request_body
            )
        else:
            print(str(e))
            print(job_start_response.text)
            raise

    # Create data directory
    try:
        os.mkdir("./_data")
    except FileExistsError:
        pass

    # Re-up token and download/upload files
    token = get_token(config)
    for proc in output:
        filename = extract_filename(proc["url"])
        local_path = download_data(
            proc["url"], token, f"./_data/{filename}_{config.partition_time}.gz"
        )
        thread = threading.Thread(
            target=write_gcs, args=(gcs_client, config, filename, local_path)
        )
        thread.start()


if __name__ == "__main__":
    main()

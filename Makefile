# ENVIRONMENT VARIABLES 

# Set this environment variables depending on use case
GOOGLE_APPLICATION_CREDENTIALS ?= ~/.ascension-cms.json
ARTIFACT_PROJECT_ID ?= gcp-stl
ARTIFACT_LOCATION ?= us-central1
ARTIFACT_REPOSITORY ?= ascension-eng-repo
ARTIFACT_IMAGE_NAME ?= bcda_ingestion_app


# Use this for only running the application code's tests
docker-test:
	@cp docker/test.Dockerfile test.Dockerfile
	@docker build --no-cache -f test.Dockerfile -t test_ingest_framework:latest .
	@rm test.Dockerfile
	@docker run -it --rm test_ingest_framework:latest
	@docker system prune -f


# Build the actual application image
docker-build:
	@cp docker/app.Dockerfile app.Dockerfile
	@docker build --no-cache -f app.Dockerfile -t ahi_ingest_framework:latest .
	@rm app.Dockerfile


# Authenticate and push the container to Artifact Registry
push-artifact-registry:
	@cat ${GOOGLE_APPLICATION_CREDENTIALS} | docker login -u _json_key \
	--password-stdin https://${ARTIFACT_LOCATION}-docker.pkg.dev
	@docker tag ahi_ingest_framework ${ARTIFACT_LOCATION}-docker.pkg.dev/${ARTIFACT_PROJECT_ID}/${ARTIFACT_REPOSITORY}/${ARTIFACT_IMAGE_NAME}:latest
	@docker push ${ARTIFACT_LOCATION}-docker.pkg.dev/${ARTIFACT_PROJECT_ID}/${ARTIFACT_REPOSITORY}/${ARTIFACT_IMAGE_NAME}:latest

# Run the BCDA application on local machine. Requires valid GCP service account.
run-bcda-app-local:
	@docker run -it --rm \
		-e BCDA_URL=${BCDA_URL} \
		-e FHIR_RESOURCE=${FHIR_RESOURCE} \
		-e GCS_BUCKET=${GCS_BUCKET} \
		-e BUCKET_PREFIX=${BUCKET_PREFIX} \
		-e PARTITION_TIME=${PARTITION_TIME} \
		-e PROJECT=${PROJECT} \
		-e SECRET_ID=${SECRET_ID} \
		-e GOOGLE_APPLICATION_CREDENTIALS=/tmp/keys/${FILE_NAME}.json \
   		-v ${GOOGLE_APPLICATION_CREDENTIALS}:/tmp/keys/${FILE_NAME}.json:ro \
		   ahi_ingest_framework:latest


# Run the BCDA application - This is the target to use for running on GKE/Cloud Run
run-bcda-app:
	@docker run -it --rm \
		-e BCDA_URL=${BCDA_URL} \
		-e FHIR_RESOURCE=${FHIR_RESOURCE} \
		-e GCS_BUCKET=${GCS_BUCKET} \
		-e BUCKET_PREFIX=${BUCKET_PREFIX} \
		-e PARTITION_TIME=${PARTITION_TIME} \
		-e PROJECT=${PROJECT} \
		-e SECRET_ID=${SECRET_ID} \
			ahi_ingest_framework:latest
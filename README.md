# Beneficiary Claims Data Ingestion

Ingests data from the [Beneficiary Claims Data API](https://bcda.cms.gov/guide.html#bcda-v2).

## Requirements

- Docker
- GCP service account with the following roles
    - Storage Writer
    - Secret Manager 
    - Secret Accessor
- GNU `make`

## Setup

It's recommended that users use the `Makefile` in the top-level directory for performing image builds and executions. Read the comments in it for target use.

1. Ensure the Docker daemon is running by opening a Terminal session and running `docker info`.
2. Fill out the variables at the top of `Makefile`. For local use, `GOOGLE_APPLICATION_CREDENTIALS` is required.
3. Run `make docker-test` to perform unit tests.
4. Run `make docker-build` to build the image.

# Running Locally

For local execution, the preferred way to run this application is with Docker. The following environment variables must be set at runtime:

| Variable | Notes |
| --- | --- |
| `BCDA_URL` | `https://sandbox.bcda.cms.gov/` should be used |
| `FHIR_RESOURCE` | Only one resource may be used per container run. Valid values are `ExplanationOfBenefit`, `Coverage`, `Patient`|
| `GCS_BUCKET` | Just the bucket name |
| `BUCKET_PREFIX` | The path to where data should be written on GCS without the bucket name, for example `bcda/coverage` would resolve to `<GCS_BUCKET>/bcda/coverage/` |
| `PARTITION_TIME` | The value by which to partition the downloaded files |
| `PROJECT` | GCP project name |
| `SECRET_ID` | The Secret Manager secret ID |
| `GOOGLE_APPLICATION_CREDENTIALS` | OPTIONAL - Used for local development; the filepath to the JSON service account file |

Once the variables are set, run `make run-bcda-app-local`.

# Deployment to Artifact Registry

The application uses GCP Artifact Registry as the preferred container registry. Users should follow the [Google documentation for setting up Docker authentication](https://cloud.google.com/artifact-registry/docs/docker/authentication). Users will need to populate `Makefile`'s `ARTIFACT_*` environment variables.

# TO-DO

BCDA app

- Add logging and send to Cloud Logging.
- Need backoff implemented on downloading process since Explanation of Benefit is flaky.
- Sandbox server has expired cert issues during individual Explanation of Benefit file downloads: `ssl.SSLError: [SSL: TLSV1_ALERT_INTERNAL_ERROR] tlsv1 alert internal error (_ssl.c:997)`
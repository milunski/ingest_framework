"""Configuration reader"""

import argparse
from typing import Literal, Optional, Text
from pydantic import (
    AnyHttpUrl,
    BaseSettings,
)


class Settings(BaseSettings):
    fhir_resource: Literal["ExplanationOfBenefit", "Coverage", "Patient"]
    bcda_url: AnyHttpUrl = "https://sandbox.bcda.cms.gov/"
    client_id: Text
    client_secret: Text
    gcs_bucket: Optional[Text]
    bucket_prefix: Optional[Text]
    partition_time: Optional[Text]
    project: Optional[Text]


def arguments(args) -> argparse.ArgumentParser:
    """Parses command-line arguments"""
    parser = argparse.ArgumentParser(
        description="Beneficiary Claims Data API V2 Ingestion Application"
    )
    parser.add_argument(
        "--bcda_url",
        help="BCDA API URL",
        nargs="?",
        const="https://sandbox.bcda.cms.gov/",
    )
    parser.add_argument("--fhir_resource", help="FHIR resource type")
    parser.add_argument("--gcs_bucket", help="GCS bucket name")
    parser.add_argument("--bucket_prefix", help="GCS bucket prefix")
    parser.add_argument("--partition_time", help="Partition time")
    parser.add_argument("--project", help="GCP project name")
    parser.add_argument("--secret_id", help="GCP Secret Manager ID")
    return parser.parse_args(args)

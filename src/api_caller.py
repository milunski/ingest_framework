"""For making API requests"""

import os
from pprint import pprint
from typing import MutableMapping, Optional, Text, Union
import urllib
import base64
import backoff
from google.cloud import storage

from pydantic import AnyHttpUrl, BaseModel, validator
import requests


from .config import Settings

# `Type to represent the id:secret combo and authorization header value
User, EncodedAuth = Text, Text
RequestHeader = MutableMapping[Text, Union[Text, EncodedAuth]]


class AccessTokenRequest(BaseModel):
    endpoint: Optional[AnyHttpUrl]
    user: Settings
    accept: Text = "application/json"

    @validator("user")
    def create_user(cls, v):
        id_secret = f"{v.client_id}:{v.client_secret}"
        return base64.b64encode(id_secret.encode()).decode("utf-8")


class JobStartRequest(BaseModel):
    """Header for starting a job"""

    endpoint: Optional[AnyHttpUrl]
    accept: Text = "application/fhir+json"
    prefer: Text = "respond-async"
    authorization: Optional[EncodedAuth]


class JobStatusRequest(BaseModel):
    """Header for checking on job status"""

    endpoint: Optional[AnyHttpUrl]
    accept: Text = "application/fhir+json"
    authorization: EncodedAuth


def url(
    settings: Settings,
    endpoint: JobStatusRequest | JobStartRequest | AccessTokenRequest,
) -> AnyHttpUrl:
    """Dynamically generates the proper URL from the config and data type of request

    Args:
        settings (Settings): Your config file represented in Pydantic BaseModel
        endpoint (JobStatusRequest | JobStartRequest | AccessTokenRequest): _description_

    Raises:
        ValueError: Raised when endpoint types aren't of the three hinted

    Returns:
        AnyHttpUrl: A URL corresponding with the request
    """
    base_url = f"{settings.bcda_url}"
    if isinstance(endpoint, JobStartRequest):
        api_export_url = "/api/v2/Group/all/$export"
        url = urllib.parse.urljoin(base_url, api_export_url)
        params = {"_type": settings.fhir_resource}
        full_url = f"{url}?{urllib.parse.urlencode(params)}"
        return full_url
    elif isinstance(endpoint, JobStatusRequest):
        jobs_url = "/api/v2/jobs"
        return urllib.parse.urljoin(base_url, jobs_url)
    elif isinstance(endpoint, AccessTokenRequest):
        auth_url = "/auth/token"
        return urllib.parse.urljoin(base_url, auth_url)
    else:
        raise ValueError("Invalid endpoint")


@backoff.on_predicate(
    wait_gen=backoff.constant,
    predicate=lambda x: "X-Progress" in x.headers.keys(),
    interval=60,
    max_time=1200,
)
def check_job_status(
    url: AnyHttpUrl, job_status: JobStatusRequest
) -> requests.Response:
    """Checks on the status of in-progress jobs

    Args:
        url (AnyHttpUrl): Endpoint for in-progress job
        job_status (JobStatusRequest): Job status settings
    Returns:
        Mapping: The response
    """
    response = requests.get(
        url,
        headers={
            "accept": job_status.accept,
            "Authorization": f"Bearer {job_status.authorization}",
        },
    )
    response.raise_for_status
    pprint(response.headers)
    return response


def extract_filename(filename: Text) -> Text:
    """Extracts the filename from the provided file path"""
    no_extension = os.path.splitext(filename)[0]
    file = no_extension.split("/")[-1]
    return file


def write_gcs(
    gcs_client: storage.Client,
    settings: Settings,
    filename: Text,
    filepath: Text,
) -> None:
    """Write contents to GCS bucket"""
    bucket = gcs_client.bucket(settings.gcs_bucket)
    blob = bucket.blob(
        f"{settings.bucket_prefix}/{filename}_{settings.partition_time}.gzip"
    )  # include file format in dest filename
    blob.upload_from_filename(filepath)


def download_data(url: AnyHttpUrl, token: Text, filename: Text) -> Text:
    """Downloading data in gzip codec

    Args:
        url (AnyHttpUrl): Data endpoint
        token (Text): Bearer token
        filename (Text): filename of fetched data
    Returns:
        Text: the destination of the file
    """
    print(f"Downloading {url}...")
    headers = {"Accept-Encoding": "gzip", "Authorization": f"Bearer {token}"}
    response = requests.get(url=url, headers=headers, stream=True)
    response.raise_for_status
    with open(filename, "wb") as f:
        f.write(response.raw.read())
    return filename


def IO_proc(
    gcs_client: storage.Client,
    settings: Settings,
    token: Text,
    filename: Text,
    url: AnyHttpUrl,
) -> None:
    local_path = download_data(
        url, token, f"./_data/{filename}_{settings.partition_time}.gz"
    )
    write_gcs(gcs_client, settings, filename, local_path)
